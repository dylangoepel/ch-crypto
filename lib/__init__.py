from Crypto.PublicKey import RSA
from Crypto.Signature import PKCS1_v1_5
from Crypto.Cipher    import PKCS1_OAEP
from Crypto.Hash      import SHA256
from base64 import b64encode, b64decode
from hashlib import sha1
from json import dumps, loads

class Key:
    def __init__(self, keyfile):
        ''' import key from keyfile '''
        with open(keyfile, "r") as f:
            self.key = RSA.importKey(f.read())
            f.close()
    def generate(length=2048):
        ''' generate new rsa key '''
        key = _Key()
        key.key = RSA.generate(length)
        return key
    def fromString(source):
        ''' import base64 encoded DER sequence of key '''
        key = _Key()
        key.key = RSA.importKey(b64decode(source.encode()))
        return key
    def export(self, target):
        ''' export key as PEM '''
        with open(target, "w") as f:
            f.write(self.key.exportKey("PEM").decode())
            f.close()
    def __str__(self):
        ''' return der base64 representation of key '''
        return b64encode(self.key.exportKey("DER")).decode()
    def sign(self, data):
        ''' return signature '''
        signer = PKCS1_v1_5.new(self.key)
        digest = SHA256.new(data)
        return b64encode(signer.sign(digest)).decode()
    def verify(self, data, signature):
        ''' verify data '''
        verifier = PKCS1_v1_5.new(self.key)
        digest = SHA256.new(data)
        return verifier.verify(digest, b64decode(signature.encode()))
    def encrypt(self, data):
        ''' return RSAES-OAEP encrypted data '''
        cipher = PKCS1_OAEP.new(self.key)
        return b64encode(cipher.encrypt(data)).decode()
    def decrypt(self, data):
        ''' decrypt RSAES-OAEP data '''
        cipher = PKCS1_OAEP.new(self.key)
        return cipher.decrypt(b64decode(data.encode("utf-8")))
    def PublicKey(self):
        key = _Key()
        key.key = self.key.publickey()
        return key

    def fingerprint(self):
        return sha1(str(self).encode()).hexdigest()

    def shortprint(self):
        return sha1(str(self).encode()).hexdigest()[:10]

class _Key(Key):
    def __init__(self):
        pass

class CryptoBlock:
    def __init__(self, val):
        if not type(val) == dict:
            raise TypeError
        self.val = val
        try:
            self.val['key']
        except KeyError:
            raise ValueError

    def hasContent(self):
        try:
            self.val['text']
            return True
        except KeyError:
            return False


    def hasSignature(self):
        try:
            self.val['sign']
            return True
        except KeyError:
            return False

    def isEncrypted(self):
        try:
            if self.val['encrypted'] == "true":
                return True
            else:
                return False
        except KeyError:
            return False

    def getKey(self):
        return Key.fromString(self.val['key'])

    def hasTargetKey(self):
        try:
            self.val['target']
            return True
        except KeyError:
            return False

    def getTargetKey(self):
        return Key.fromString(self.val['target'])

    def verify(self):
        if not self.hasSignature():
            raise TypeError("No signature available")
        elif not self.hasContent():
            raise TypeError("No content available")
        try:
            return self.getKey().verify(self.val['text'].encode("utf-8"), self.val['sign'])
        except:
            return False

    def __str__(self):
        return dumps(self.val)

    def __dict__(self):
        return self.val

    def decrypt(self, key):
        try:
            if self.val['encrypted'] != "true":
                raise KeyError 
            if key.PublicKey().fingerprint() != self.getTargetKey().fingerprint():
                raise ValueError
            return key.decrypt(b64decode(self.val['text'].encode("utf-8")).decode("utf-8"))
        except KeyError:
            return self.val['text'].encode("utf-8")

    def fromString(source):
        arr = loads(source)
        if arr['type'] != "crypto":
            raise TypeError("Not a crypto block")

        self = CryptoBlock.fromDict(arr)
        return self

    def fromDict(source):
        self = _CryptoBlock()
        self.val = source
        return self

class _CryptoBlock(CryptoBlock):
    def __init__(self):
        pass

class SignedMessage(CryptoBlock):
    def __init__(self, key, text, target=False):
        self.val = {}
        self.val['key'] = str(key.PublicKey())
        self.val['text'] = text
        self.val['sign'] = key.sign(text.encode("utf-8"))
        if target:
             self.val['target'] = str(target)

class EncryptedMessage(CryptoBlock):
    def __init__(self, key, text, target, sign=True):
        self.val = {}
        self.val['key'] = str(key.PublicKey())
        self.val['text'] = b64encode(target.encrypt(text.encode("utf-8")).encode("utf-8")).decode("utf-8")
        self.val['encrypted'] = "true"
        self.val['target'] = str(target.PublicKey())
        if sign:
            sign = key.sign(self.val['text'].encode("utf-8"))
            self.val['sign'] = sign
